import pytest
from src.dataset import emotions, emotions_local


@pytest.fixture()
def emotions_data():
    return emotions()


@pytest.fixture()
def emotions_local_data():
    return emotions_local()
