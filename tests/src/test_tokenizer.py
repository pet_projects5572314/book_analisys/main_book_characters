import pytest

from src.tokenizer import tokenizer, tokenize, tokenized_local


def test_str_to_token_id():
    input = "I want create my own NLP project"
    output = {
        "input_ids": [101, 1045, 2215, 3443, 2026, 2219, 17953, 2361, 2622, 102],
        "attention_mask": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    }
    assert str(tokenizer(input)) == str(output)


def test_token_id_to_str():
    tokens = tokenizer("I want create my own NLP project")
    input = tokenizer.convert_ids_to_tokens(tokens.input_ids)
    output = [
        "[CLS]",
        "i",
        "want",
        "create",
        "my",
        "own",
        "nl",
        "##p",
        "project",
        "[SEP]",
    ]
    assert str(input) == str(output)


def test_tokenaze(emotions_data):
    input = tokenize(emotions_data["train"][:2])
    output = {
        "input_ids": [
            [
                101,
                1045,
                2134,
                2102,
                2514,
                26608,
                102,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            [
                101,
                1045,
                2064,
                2175,
                2013,
                3110,
                2061,
                20625,
                2000,
                2061,
                9636,
                17772,
                2074,
                2013,
                2108,
                2105,
                2619,
                2040,
                14977,
                1998,
                2003,
                8300,
                102,
            ],
        ],
        "attention_mask": [
            [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
    }
    assert str(input) == str(output)


def test_load_tokenazed_data(emotions_data):
    input = tokenized_local()
    output = "DatasetDict({\n    train: Dataset({\n        features: ['text', 'label', 'input_ids', 'attention_mask'],\n        num_rows: 16000\n    })\n    validation: Dataset({\n        features: ['text', 'label', 'input_ids', 'attention_mask'],\n        num_rows: 2000\n    })\n    test: Dataset({\n        features: ['text', 'label', 'input_ids', 'attention_mask'],\n        num_rows: 2000\n    })\n})"
    assert str(input) == output
