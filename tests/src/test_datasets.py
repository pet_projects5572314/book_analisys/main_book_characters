import pytest

from src.dataset import emotions, emotions_local


def test_dataset_shape(emotions_data):
    print(emotions_data)

    output = """DatasetDict({\n    train: Dataset({\n        features: ['text', 'label'],\n        num_rows: 16000\n    })\n    validation: Dataset({\n        features: ['text', 'label'],\n        num_rows: 2000\n    })\n    test: Dataset({\n        features: ['text', 'label'],\n        num_rows: 2000\n    })\n})"""

    assert str(emotions_data) == output


def test_emotions_dataset_type(emotions_data):
    train = emotions_data["train"]
    validation = emotions_data["validation"]
    test = emotions_data["test"]

    output = "{'text': Value(dtype='string', id=None), 'label': ClassLabel(names=['sadness', 'joy', 'love', 'anger', 'fear', 'surprise'], id=None)}"

    assert str(train.features) == output
    assert str(validation.features) == output
    assert str(test.features) == output


def test_emotions_local_dataset_type(emotions_local_data):
    train = emotions_local_data["train"]
    validation = emotions_local_data["validation"]
    test = emotions_local_data["test"]

    output = "{'text': Value(dtype='string', id=None), 'label': ClassLabel(names=['sadness', 'joy', 'love', 'anger', 'fear', 'surprise'], id=None)}"

    assert str(train.features) == output
    assert str(validation.features) == output
    assert str(test.features) == output


def test_compare_emotions_vs_emotions_local_dataset_type(
    emotions_data, emotions_local_data
):
    assert emotions_data["train"].features == emotions_local_data["train"].features
    assert (
        emotions_data["validation"].features
        == emotions_local_data["validation"].features
    )
    assert emotions_data["test"].features == emotions_local_data["test"].features
