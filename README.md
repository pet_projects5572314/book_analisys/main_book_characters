# main_book_characters

This library is created for find and create summarises of main characters from book.

## Get and create local txt data
```bash
python src/get_dataset.py
```
## Load dataset
### for dataset from Hugging Face hub
```python
from src.dataset import emotions
emotions_data = emotions()
```

### for dataset from local txt files (first need create it by get_dataset.py)
```python
from src.dataset import emotions_local
emotions_local_data = emotions_local()
```

## Preprocessing
### Load tokenizer
```python
from src.tokenazer import tokenizer
test = "I want create my own NLP project"
tokenizer(test)
```

### store tokenized dataset
```bash
python src/get_tokenize_dataset.py
```

### load stored tokenized dataset
```python
from src.tokenizer import tokenized_local
tokenized_data = tokenized_local()
```

