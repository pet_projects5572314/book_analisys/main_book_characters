from src.tokenizer import tokenize
from src.dataset import emotions_local

emotions = emotions_local()
emotions_encoded = emotions.map(tokenize, batched=True, batch_size=None)  # type: ignore

path = "data/intermediate/emotions_tokenized/"
emotions_encoded.save_to_disk(path)  # type: ignore
print(f"Save to {path}")
