from datasets import ClassLabel, load_dataset

# get dataset from Hub
emotions = lambda: load_dataset("emotion")


# Load datasets from txt
def emotions_local():
    path = "data/raw/emotions/"
    files = {
        "train": f"{path}train.txt",
        "validation": f"{path}validation.txt",
        "test": f"{path}validation.txt",
    }
    emotions_local = load_dataset(
        "csv", data_files=files, sep=";", names=["text", "label"]
    )
    emotions_local = emotions_local.cast_column(
        "label",
        ClassLabel(names=["sadness", "joy", "love", "anger", "fear", "surprise"]),
    )
    return emotions_local


if __name__ == "__main__":
    print(str(emotions()))
    print(str(emotions_local()))
