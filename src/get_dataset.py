import os

from datasets import load_dataset

# get data from url
# path_train = "data/raw/train.txt"
# if not os.path.isfile(path_train):
#     dataset_url = "https://huggingface.co/datasets/transformersbook/emotion-train-split/raw/main/train.txt"
#     command = f"wget {dataset_url} -O {path_train}"
#     os.system(command)
# else:
#     print(f"{path_train} is already exists ")

path = "data/raw/emotions/"
splits = ["train.txt", "validation.txt", "test.txt"]
file_list = [path + split for split in splits]

if not all([os.path.isfile(f) for f in file_list]):
    emotions = load_dataset("emotion")

    for split, dataset in emotions.items():  # type: ignore
        dataset.to_csv(f"{path}{split}.txt", index=None, header=False, sep=";")
else:
    print(f"All files are already exists")
