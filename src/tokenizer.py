# hide_output
from transformers import AutoTokenizer
from datasets import load_from_disk

model_ckpt = "distilbert-base-uncased"
tokenizer = AutoTokenizer.from_pretrained(model_ckpt)
print("load distilbert-base-uncased tokenazer")


def tokenize(batch):
    return tokenizer(batch["text"], padding=True, truncation=True)


path = "data/intermediate/emotions_tokenized/"
tokenized_local = lambda: load_from_disk(path)


if __name__ == "__main__":
    ##############################
    text = "I want create my own NLP project"
    ##############################

    # tokenaze text by toeknazer
    encoded_text = tokenizer(text)
    print(encoded_text)
    print(f"encoded_text: {type(encoded_text)}")

    # get tokens
    tokens = tokenizer.convert_ids_to_tokens(encoded_text.input_ids)
    print(tokens)

    # print(f"Input tensor shape: {encoded_text['input_ids'].size()}")
    # text = "this is a test"
    # inputs = tokenizer(text, return_tensors="pt")
    # print(f"Input tensor shape: {inputs['input_ids'].size()}")
    # print(f"encoded_text: {type(inputs)}")
